package com.example.parse_xml;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        findViewById(R.id.my_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    testXML();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void testXML() throws Exception {
        AssetManager assetManager = getAssets();
        try {
            InputStream xmlStream = assetManager.open("h4.xml");
            try {
                XmlPullParser xmlParser = Xml.newPullParser();
                xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                xmlParser.setInput(xmlStream, null);
                xmlParser.nextTag();

                int imgNum = 0;
                xmlParser.require(XmlPullParser.START_TAG, null, "root");
                while (xmlParser.next() != XmlPullParser.END_TAG) {
                    if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }

                    xmlParser.require(XmlPullParser.START_TAG, null, "entry");
                    String value1 = xmlParser.getAttributeValue(null, "key1");
                    Log.d("TAG", value1.toString());
                    xmlParser.nextTag();
                    xmlParser.require(XmlPullParser.END_TAG, null, "entry");
                }



            } finally {
                xmlStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assetManager.close();
    }
}